import 'package:flutter/material.dart';

import '../screens/category_meals_screen.dart';
import '../screens/meal_detail_screen.dart';
import '../screens/tabs_scressn.dart';
import '../screens/filters_screen.dart';

import '../models/dummy_data.dart';
import '../models/meal.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegetarian': false,
    'vegan': false,
  };

  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoriteMeals = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableMeals = DUMMY_MEALS.where((meal) {
        if (_filters['gluten'] as bool && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] as bool && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegetarian'] as bool && !meal.isVegetarian) {
          return false;
        }
        if (_filters['vegan'] as bool && !meal.isVegan) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorit(String mealId) {
    final mealIndex = _favoriteMeals.indexWhere((meal) => meal.id == mealId);

    if (mealIndex == -1) {
      final meal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);
      setState(() {
        _favoriteMeals.add(meal);
      });
    } else {
      setState(() {
        _favoriteMeals.removeAt(mealIndex);
      });
    }
  }

  bool _isFavorit(String mealId) {
    return _favoriteMeals.any((meal) => meal.id == mealId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meal App',
      theme: ThemeData(
        // primarySwatch: Colors.pink,
        // accentColor: Colors.amber,
        colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.pink)
            .copyWith(secondary: Colors.amber),
        canvasColor: const Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontFamily: 'RobotoCondensed',
              ),
            ),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', //Default is '/', but if not can be set throu this line
      routes: {
        '/': (context) => TabsScreen(
              availableMeals: _availableMeals,
              favoritMeals: _favoriteMeals,
            ),
        CategoryMealsScreen.routeName: (context) =>
            CategoryMealsScreen(availableMealsList: _availableMeals),
        MealDetailScreen.routeName: (context) => MealDetailScreen(
            toggleFavorit: _toggleFavorit, isFavorit: _isFavorit),
        FiltersScreen.routeName: (context) =>
            FiltersScreen(filters: _filters, setFilters: _setFilters),
      },

      //run if a route is not defined and
      //instead of that wil push de screen bellow to the route stack.
      onGenerateRoute: (setting) {
        return MaterialPageRoute(
          builder: (context) => TabsScreen(
            availableMeals: _availableMeals,
            favoritMeals: _favoriteMeals,
          ),
        );
      },

      // if the app can not build a screen because the route is unknown and/or
      // onGenerateRoute fails and/or de default route "/" faild, then flutter try
      //to show somting. this can be for example used for 404 screen.
      onUnknownRoute: (setting) {
        return MaterialPageRoute(
          builder: (context) => TabsScreen(
            availableMeals: _availableMeals,
            favoritMeals: _favoriteMeals,
          ),
        );
      },

      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DeliMeals'),
      ),
      body: const Center(
        child: Text('Navigation Time!'),
      ),
    );
  }
}
