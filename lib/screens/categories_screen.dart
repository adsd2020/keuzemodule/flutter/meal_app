import 'package:flutter/material.dart';

import '../widgets/category_item.dart';

import '../models/meal.dart';
import '../models/dummy_data.dart';

class CategoriesScreen extends StatelessWidget {
  final List<Meal> mealList;
  const CategoriesScreen({Key? key, required this.mealList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> categoryIds = mealList
        .map((meal) => meal.categories)
        .expand((catIds) => catIds)
        .toSet()
        .toList();

    return GridView(
      children: DUMMY_CATEGORIES
          .where((cat) => categoryIds.contains(cat.id))
          .map(
            (catData) => CategoryItem(
              id: catData.id,
              title: catData.title,
              color: catData.color,
            ),
          )
          .toList(),
      padding: const EdgeInsets.all(25),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 1.5,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
