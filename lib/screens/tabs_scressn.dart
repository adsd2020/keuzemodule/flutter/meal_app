import 'package:flutter/material.dart';

import './categories_screen.dart';
import './favoriets_screen.dart';

import '../widgets/main_drawer.dart';

import '../models/meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> availableMeals;
  final List<Meal> favoritMeals;
  
  const TabsScreen(
      {Key? key, required this.availableMeals, required this.favoritMeals})
      : super(key: key);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  late List<Map<String, Object>> _pages;
  int _selectedPage = 0;

  void _selectPage(int index) {
    setState(() {
      _selectedPage = index;
    });
  }

  @override
  void initState() {
    _pages = [
      {
        'page': CategoriesScreen(mealList: widget.availableMeals),
        'title': 'Meal Categories'
      },
      {
        'page': FavorietsScreen(favoritMeals: widget.favoritMeals),
        'title': 'Your Favoriets'
      },
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPage]['title'] as String),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPage]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPage,
        // type: BottomNavigationBarType.shifting,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: 'Categories',
            // backgroundColor: Theme.of(context).colorScheme.primary,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Favoriets',
            // backgroundColor: Theme.of(context).colorScheme.primary,
          )
        ],
        backgroundColor: Theme.of(context).colorScheme.primary,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
      ),
    );
  }
}
