import 'package:flutter/material.dart';

import '../widgets/meal_itam.dart';

import '../models/meal.dart';

class FavorietsScreen extends StatelessWidget {
  final List<Meal> favoritMeals;

  const FavorietsScreen({Key? key, required this.favoritMeals})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (favoritMeals.isEmpty) {
      return const Center(
        child: Text('You have no favoriets yet - start adding some!'),
      );
    } else {
      return ListView.builder(
          itemBuilder: (ctx, index) {
            return MealItem(
              id: favoritMeals[index].id,
              title: favoritMeals[index].title,
              imageUrl: favoritMeals[index].imageUrl,
              duration: favoritMeals[index].duration,
              complexity: favoritMeals[index].complexity,
              affordability: favoritMeals[index].affordability,
            );
          },
          itemCount: favoritMeals.length,
        );
    }
  }
}
