import 'package:flutter/material.dart';

import '../models/dummy_data.dart';

class MealDetailScreen extends StatelessWidget {
  static const String routeName = "/meal-detail";

  final Function toggleFavorit;
  final Function isFavorit;

  const MealDetailScreen({Key? key, required this.toggleFavorit, required this.isFavorit}) : super(key: key);

  Widget _buildSectionTitle(
    BuildContext context,
    String _title,
    double heightValue,
  ) {
    return Container(
      height: heightValue,
      alignment: Alignment.center,
      child: Text(
        _title,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget _buildContainer(
    Widget child,
    double heightValue,
    double widthValue,
  ) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.only(bottom: 10),
      height: heightValue,
      width: widthValue,
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final String _mealId = ModalRoute.of(context)!.settings.arguments as String;

    final _selectedMeal = DUMMY_MEALS.firstWhere((meal) => meal.id == _mealId);

    final _appBar = AppBar(
      title: Text(_selectedMeal.title),
    );
    final _mediaQuery = MediaQuery.of(context);
    final _screenHeight = _mediaQuery.size.height -
        _appBar.preferredSize.height -
        _mediaQuery.padding.vertical;

    return Scaffold(
      appBar: _appBar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: _screenHeight * 0.4,
              width: double.infinity,
              child: Image.network(
                _selectedMeal.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            _buildSectionTitle(
              context,
              'Ingredients',
              _screenHeight * 0.06,
            ),
            _buildContainer(
              ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      child: Text(_selectedMeal.ingredients[index]),
                    ),
                    color: Theme.of(context).colorScheme.secondary,
                  );
                },
                itemCount: _selectedMeal.ingredients.length,
              ),
              _screenHeight * 0.20,
              _mediaQuery.size.width * 0.8,
            ),
            _buildSectionTitle(
              context,
              'Steps',
              _screenHeight * 0.06,
            ),
            _buildContainer(
              ListView.builder(
                itemCount: _selectedMeal.steps.length,
                itemBuilder: (context, index) => Column(
                  children: [
                    ListTile(
                      leading: CircleAvatar(
                        child: Text(
                          '#${index + 1}',
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        backgroundColor: Theme.of(context).colorScheme.primary,
                      ),
                      title: Text(_selectedMeal.steps[index]),
                    ),
                    const Divider(),
                  ],
                ),
              ),
              _screenHeight * 0.25,
              _mediaQuery.size.width * 0.8,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(isFavorit(_mealId)?Icons.star:Icons.star_border),
        onPressed: () => toggleFavorit(_mealId),
        // () {
        //   Navigator.of(context).pop(_mealId);
        // },
      ),
    );
  }
}
